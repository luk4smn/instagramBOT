require 'watir'                 # Crawler
require 'pry'                   # Ruby REPL
require 'rb-readline'           # Ruby IRB
require 'awesome_print'         # Console output
require_relative 'credentials'  # Pulls in login credentials from credentials.rb

username = $username
password = $password
users = $users

follow_counter = 0
unfollow_counter = 0
MAX_FOLLOWS = 1000
MAX_UNFOLLOWS = 1000
start_time = Time.now

# Open Browser, Navigate to Login page
browser = Watir::Browser.new :chrome, switches: ['--incognito']
browser.goto "instagram.com/accounts/login/"

# Navigate to Username and Password fields, inject info
puts "Logging in..."
browser.text_field(:name => "username").set "#{username}"
browser.text_field(:name => "password").set "#{password}"

# Click Login Button
if browser.button(:tag_name => 'form button').exist?
  # browser.button(:tag_name => 'form button').click

  browser.driver.execute_script("document.querySelectorAll('button[type=submit]')[0].click()");
end

sleep(20)

# Continuous loop to run until you've unfollowed the max people for the day
loop do
  users.each { |val|
    # Navigate to user's page
    browser.goto "instagram.com/#{val}/"

    if browser.browser.button(:tag_name => 'header section div span span').exists?
      browser.button(:tag_name => 'header section div span span').click
      # If not following then follow
      if browser.button(:class => 'aOOlW').exists?
        begin
          browser.button(:class => 'aOOlW').click
          ap "Unfollowing #{val}"
          unfollow_counter += 1
          unfollow_counter % 10 == 0 ? sleep(30) : false
        rescue
          sleep(1)
        end
      else
        begin
          ap "following #{val}"
          follow_counter += 1
          follow_counter % 10 == 0 ? sleep(60) : false
        rescue
          sleep(2)
        end
      end

    end
    sleep(1.0/2.0) # Sleep half a second to not get tripped up when un/following many users at once
  }
  puts "--------- #{Time.now} ----------"
  puts "Waiting for process again"
  break if unfollow_counter >= MAX_UNFOLLOWS || follow_counter >= MAX_FOLLOWS
  sleep(60) # Sleep 1 hour (3600 seconds)
end

ap "Followed #{follow_counter} users and unfollowed #{unfollow_counter} in #{((Time.now - start_time)/60).round(2)} minutes"

# Leave this in to use the REPL at end of program
# Otherwise, take it out and program will just end
