# Change these to your login info

$username = "login"
$password = "password"

$users = [
    'theweeknd', 'kendalljenner', 'karliekloss', 'instagram', 'pizzariadnapoles','sikerajr', 'luanestilizado', 'thiagosilva_33', 'paollaoliveirareal',
    'selenagomez', 'arianagrande', 'taylorswift', 'beyonce', 'kimkardashian', 'pizzariadobeguinha','korpusacademia','esporteinterativo', 'sabrinasato', 'claudiaalende',
    'cristiano', 'kyliejenner', 'justinbieber', 'kendalljenner', 'nickiminaj', 'pizzariaguimaraes', 'spazziocampina', 'luciano', 'jujusalimeni','eliana', 'brunogagliasso',
    'felipeneto', 'ivetesangalo', 'caiocastro', 'martingarrix', 'lucaslucco', 'gisele', 'claudialeitte', 'kaka', 'gio_ewbank', 'julianapaes', 'maisa', 'adrianalima', 'paulogustavo31',
    'natgeo', 'neymarjr', 'nike', 'leomessi','khloekardashian', 'mileycyrus', 'expresspizzariajp', 'barducoadvogados', 'lucianohuck', 'ironmaiden', 'massafera', 'gusttavolima',
    'katyperry', 'jlo', 'ddlovato', 'kourtneykardash', 'victoriassecret', 'badgalriri', 'campinaprime','oficialavioes', 'pipocando.oficial', 'avatimoficial', 'angelicaksy',
    'fcbarcelona', 'realmadrid', 'theellenshow', 'justintimberlake', 'zendaya', 'caradelevingne','advocaciaempauta', 'xandaviao', 'fecastanhari', 'boiebrasacampinagrande', 'marcosmion',
    '9gag', 'chrisbrownofficial', 'vindiesel', 'champagnepapi', 'davidbeckham', 'shakira', 'gigihadid', 'vpbg.advogados', 'cauemoura', 'larissamanoela', 'rodrigofaro', 'jorgeemateus',
    'emmawatson', 'jamesrodriguez10', 'kingjames', 'garethbale11', 'nikefootball', 'adele', 'zacefron', 'advogadosbauru','wesleysafadao', 'cellbitos', 'avatimcampinagrande', 'mariliamendoncacantora',
    'vanessahudgens', 'ladygaga', 'maluma', 'nba', 'nasa', 'ronaldinho', 'luissuarez9', 'zayn', 'shawnmendes', 'advogadostemporarios', 'danilogentili', 'campinamais', 'alessandraambrosio',
    'adidasfootball', 'brumarquezine', 'hm', 'harrystyles','chanelofficial', 'ayutingting92', 'letthelordbewithyou','direitonoempreendedorismo', 'colcci_campinagrande', 'simoneses',
    'niallhoran', 'anitta', 'hudabeauty', 'camerondallas', 'adidasoriginals', 'marinaruybarbosa', 'lucyhale', 'karimbenzema', 'rubinoadvogados', 'eucurtocampina', 'isisvalverde',
    'princessyahrini', 'zara', 'nickyjampr', 'onedirection', 'andresiniesta8', 'raffinagita1717', 'krisjenner', 'manchesterunited', 'tatico_operacional', 'kefera', 'ronaldo',
    'natgeotravel', 'marcelotwelve', 'deepikapadukone', 'snoopdogg', 'davidluiz_4', 'priyankachopra', 'ashleybenson', 'trezefcoficial', 'keferalifestyle', 'dedesecco',
    'preparatorio_defensorias', 'lelepons', 'prillylatuconsina96','louisvuitton','britneyspears', 'saobrazcafe', 'jbalvin', 'laudyacynthiabella', 'ciara', 'cafajestando', 'barbosa.adv',
    'stephencurry30', 'instagrambrasil', 'carlinhosmaiaof', 'whinderssonnunes', 'tirullipa', 'expresspizzaria', 'campinenseclube','cbnparaiba', 'chloegmoretz', 'camilaqueiroz',
    'campinamais', 'campinagrill', 'maiorsjdomundo', 'saojoaodecampinagrande', 'ranieri_nogueira', 'fabrinni_brito', 'advogadosdigitais', 'cbnoficial', 'phil.coutinho', 'cocielo',
    'simoneesimaria','joaoguilherme', 'arthuraguiar', 'danirussotv', 'tatafersoza', 'isabellasantoni', 'flaapavanelli', 'cauareymond', 'marianarios', 'ahickmann', 'juulianapaiva', 'micheltelo',
    'nahcardoso', 'mcguioficial', 'aline_riscado', 'pabllovittar', 'xuxamenegheloficial', 'rafaavitti', 'evaristocostaoficial', 'christian_fig', 'gioanto', 'biancaandradeoficial', 'gabrielmedina',
    'luisasonza', 'gustavorochaa', 'graoficial', 'henriqueejuliano', 'dejesusoficial', 'gilancellotti', 'oscar_emboaba', 'thbarbosa', 'sbtonline', 'flaviaalereal', 'pretagil', 'taisdeverdade',
    'araujovivianne', 'simasrodrigo', 'willianborges88', 'otacosta', 'muniknunes', 'leosantana', 'programapanico', 'majutrindade', 'oficialkellykey', 'lojasamericanas', 'walmart_br', 'casasbahia',
    'magazineluiza', 'armazempb', 'mcdonalds_br', 'bobsbrasil', 'burgerkingbrasil'
]

$tags = [
    'direitodoconsumidor', 'consumidor', 'procon', 'direitopenal', 'direitoporamor', 'advogados', 'advocacia',
    'advogadotrabalhista', 'advocaciacriminal', 'advocaciatrabalhista', 'criminalista', 'pf', 'plantao',
    'campinagrande', 'partageshopping', 'joaopessoa','saopaulo','rj', 'sp','bahia','salvador', 'brasil',
    'oab','br','direito','followme','fitnessbr', 'oabpb','oabsp','oabrj','oabsc','oabes','oabba','oabpe',
    'advogados','direito', 'trabalhista', 'varadafamilia','direitomilitar','conciliacao','direitocivil',
    'defensoriapublica', 'defensoria', 'oabal', 'oabam',
]