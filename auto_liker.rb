require 'watir' # Crawler
require 'pry' # Ruby REPL
require 'rb-readline' # Ruby IRB
require 'awesome_print' # Console output
require_relative 'credentials' # Pulls in login credentials from credentials.rb

username = $username
password = $password
like_counter = 0
num_of_rounds = 0
MAX_LIKES = 1500

# Open Browser, Navigate to Login page
browser = Watir::Browser.new :chrome, switches: ['--incognito']
browser.goto "instagram.com/accounts/login/"

# Navigate to Username and Password fields, inject info
puts "Logging in..."
browser.text_field(:name => "username").set "#{username}"
browser.text_field(:name => "password").set "#{password}"

# Click Login Button
if browser.button(:tag_name => 'form button').exist?
  # browser.button(:tag_name => 'form button').click
  browser.driver.execute_script("document.querySelectorAll('button[type=submit]')[0].click()");
end
sleep(10)

if browser.button(:class => 'aOOlW').exists?
  browser.button(:class => 'aOOlW').click
end

browser.goto "instagram.com/"


# Continuous loop to break upon reaching the max likes
loop do
  # Scroll to bottom of window 3 times to load more results (20 per page)
  3.times do |i|
    browser.driver.execute_script("window.scrollBy(0,document.body.scrollHeight)")
    sleep(1)
  end

  currentLikes = like_counter

  # Call all unliked like buttons on page and click each one.
  if browser.span(:class => 'glyphsSpriteHeart__outline__24__grey_9').exists?

    browser.spans(:class => 'glyphsSpriteHeart__outline__24__grey_9').each { |val|
      begin
        val.click
        like_counter += 1
      rescue
        3.times do |i|
          browser.driver.execute_script("window.scrollBy(0,10)")
          sleep(1)
        end
      ensure
        if currentLikes > 0 && currentLikes == like_counter
          browser.goto "instagram.com/"
          sleep(20)
          browser.driver.execute_script("window.scrollBy(0,document.body.scrollHeight)")
        end
      end
    }
    ap "Photos liked: #{like_counter}"
  else
    ap "No media to like."
    if currentLikes > 0 && browser.a(:class => 'coreSpriteGlyphBlack').exist?
      browser.a(:class => 'coreSpriteGlyphBlack').click
    end

  end
  num_of_rounds += 1
  puts "--------- #{like_counter / num_of_rounds} likes per round (on average) ----------"
  break if like_counter >= MAX_LIKES
  sleep(30) # Return to top of loop after this many seconds to check for new photos
end

# Leave this in to use the REPL at end of program
# Otherwise, take it out and program will just end
Pry.start(binding)
