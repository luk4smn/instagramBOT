# InstaBOT

### Credentials
Make sure to update your login info in the credentials.rb file.

### How to Run from Command Line
To run each file navigate to the main directory (web_scraper) in your terminal and run the word `ruby` followed by the name of the file. Example:
`$ ruby auto_follow.rb`
or
`$ ruby auto_liker.rb`

### Libraries Used
[watir](https://github.com/watir/watir) for crawling in live browser<br>
[pry](https://github.com/pry/pry) ruby REPL<br>
[rb-readline](https://github.com/ConnorAtherton/rb-readline) ruby IRB and dependency of pry<br>
[awesome-print](https://github.com/awesome-print/awesome_print) for clearer console output
