require 'watir' # Crawler
require 'pry' # Ruby REPL
require 'rb-readline' # Ruby IRB
require 'awesome_print' # Console output
require_relative 'credentials' # Pulls in login credentials from credentials.rb

username = $username
password = $password
follow_counter = 0
num_of_rounds = 0
MAX_FOLLOWS_1 = 150

# Open Browser, Navigate to Login page
browser = Watir::Browser.new :chrome, switches: ['--incognito']
browser.goto "instagram.com/accounts/login/"

# Navigate to Username and Password fields, inject info
puts "Logging in..."
browser.text_field(:name => "username").set "#{username}"
browser.text_field(:name => "password").set "#{password}"

# Click Login Button
if browser.button(:tag_name => 'form button').exist?
  # browser.button(:tag_name => 'form button').click
  browser.driver.execute_script("document.querySelectorAll('button[type=submit]')[0].click()");
end
sleep(10)

if browser.button(:class => 'aOOlW').exists?
  browser.button(:class => 'aOOlW').click
end

browser.goto "instagram.com/explore/people/suggested/"

# Continuous loop to break upon reaching the max likes
loop do
  browser.goto "instagram.com/explore/people/suggested/"
  sleep(2)

  # Call all unliked like buttons on page and click each one.
  if browser.button(:tag_name => 'main div ul div li button').exists?

    browser.buttons(:tag_name => 'main div ul div li button').each { |val|
      # ap val
      val.click
      follow_counter +=1
    }

  end

  num_of_rounds += 1
  puts "--------- #{num_of_rounds} Round ----------"
  break if follow_counter >= MAX_FOLLOWS_1
  sleep(60) # Return to top of loop after this many seconds to check for new photos
end

