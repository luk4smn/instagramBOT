require 'watir' # Crawler
require 'pry' # Ruby REPL
require 'rb-readline' # Ruby IRB
require 'awesome_print' # Console output
require_relative 'credentials' # Pulls in login credentials from credentials.rb

username = $username
password = $password
tags     = $tags
like_counter = 0
MAX_LIKES_BY_HASHTAG = 100

# Open Browser, Navigate to Login page
browser = Watir::Browser.new :chrome, switches: ['--incognito']
browser.goto "instagram.com/accounts/login/"

# Navigate to Username and Password fields, inject info
puts "Logging in..."
browser.text_field(:name => "username").set "#{username}"
browser.text_field(:name => "password").set "#{password}"

# Click Login Button
if browser.button(:tag_name => 'form button').exist?
  # browser.button(:tag_name => 'form button').click

  browser.driver.execute_script("document.querySelectorAll('button[type=submit]')[0].click()");
end
sleep(10)

if browser.button(:class => 'aOOlW').exists?
  browser.button(:class => 'aOOlW').click
end

# document.querySelectorAll('[role="dialog"]')[2]
# document.getElementsByTagName('img')

loop do
  tags.each { |val|
    # Navigate to hashtag's page
    browser.goto "instagram.com/explore/tags/#{val}/"
    round = 0
    likes = 0

    browser.as.each do |val1|
      if round == 1
        val1.click

        loop do
          begin
            browser.button(:class => 'coreSpriteHeartOpen').click
            browser.a(:class => 'coreSpriteRightPaginationArrow').click
            likes += 1
            sleep(2)
          rescue
            begin
              browser.a(:class => 'coreSpriteRightPaginationArrow').click
            rescue
              break
            end
          ensure
            break if likes == MAX_LIKES_BY_HASHTAG
          end
        end

      end
      round += 1
    end

    ap "#{val} - Photos liked: #{likes}"
    sleep(1.0/2.0) # Sleep half a second to not get tripped up when un/following many users at once
  }
  puts "--------- #{Time.now} ----------"
  puts "Waiting for process again"
  sleep(60) # Sleep 1 hour (3600 seconds)
end